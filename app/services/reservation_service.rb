module ReservationService
  class << self
    def create_reservation(user_id, date)
      Reservation.create(user_id: user_id, reservation_on: date)
    end
  end
end
