class UserService
  class << self
    def find_or_create(slack_id, slack_client)
      User.find_or_create_by(slack_id: slack_id) do |user|
        user.name = slack_client.web_client.users_info(user: slack_id)['user']['real_name']
      end
    end
  end
end
