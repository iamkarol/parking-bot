class MindSpaceParkingBot < SlackRubyBot::Bot
  help do
    title 'MIND SPACE PARKING BOT'
    desc 'This bot helps you reserve parking spot for a given date.'

    command 'list' do
      desc 'Returns list of reservation starting from today.'
    end

    command 'free <date>' do
      desc 'Tells you if there are any reservation for a given <date>.'
    end

    command 'reserve <date>' do
      desc 'Makes you a reservation for a given <date>.'
    end

    command 'cancel <date>' do
      desc 'Cancels your reservation for a given <date>.'
    end

    command 'stats' do
      desc 'Returns stats of reservations for each user.'
    end
  end

  command 'list' do |client, data, match|
    reservations = Reservation.from_today

    message = "List of reservations starting from today:\n"
    if reservations.blank?
      message += "There are no reservations.\n"
    else
      reservations.each do |reservation|
        message += " • #{DateHandler.format_date(reservation.reservation_on)} reserved by *#{reservation.user.name}*\n"
      end
    end
    client.say(text: message, channel: data.channel)
  end

  command 'stats' do |client, data, match|
    users = User.all

    message = "Stats for all users:\n"
    if users.blank?
      message += "There are no active users.\n"
    else
      users.each do |user|
        message += " • #{user.name} had *#{user.reservations.count}* reservations\n"
      end
    end
    client.say(text: message, channel: data.channel)
  end

  match /free\s*(?<date>.*)/i do |client, data, match|
    date = DateHandler.parse_date(match[:date])
    break client.say(text: 'No date given.', channel: data.channel) if date.blank?

    reservation = Reservation.find_by(reservation_on: date)

    message = if reservation.blank?
      "There are no reservation on #{DateHandler.format_date(date)}"
    else
      "There is following reservation: #{reservation.user.name} on #{DateHandler.format_date(date)}"
    end
    client.say(text: message, channel: data.channel)
  end

  match /reserve\s?(?<date>.*)/i do |client, data, match|
    date = DateHandler.parse_date(match[:date])
    break client.say(text: 'No date given.', channel: data.channel) if date.blank?

    reservation = ReservationService.create_reservation(UserService.find_or_create(data.user, client).id, date)

    message = if reservation&.persisted?
      ":rollo: I have reserved parking spot for *#{reservation.user.name}* on: #{DateHandler.format_date(date)}."
    else
      ":very_thinking: Sorry, reservation is not possible for #{DateHandler.format_date(date)}."
    end

    client.say(text: message, channel: data.channel)
  end

  match /cancel\s?(?<date>.*)/i do |client, data, match|
    date = DateHandler.parse_date(match[:date])
    break client.say(text: 'No date given.', channel: data.channel) if date.blank?

    reservation = Reservation.find_by(user: UserService.find_or_create(data.user, client), reservation_on: date)

    message = if reservation.blank?
      "There are no reservations for you on #{DateHandler.format_date(date)}. Nothing to cancel."
    else
      reservation.delete
      "I have cancelled your reservation on #{DateHandler.format_date(date)}. "
    end

    client.say(text: message, channel: data.channel)
  end

  command 'ping' do |client, data, match|
    client.say(text: 'pong', channel: data.channel)
  end
end
