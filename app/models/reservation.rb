class Reservation < ApplicationRecord
  belongs_to :user
  validates :reservation_on, uniqueness: true

  scope :from_today, -> { where('reservation_on >= ?', Date.today) }

  def for_day(date)
    find_by(reservation_on: date)
  end
end
