module DateHandler
  class << self
    def format_date(date)
      date.strftime('%A, %B %-d, %Y')
    end

    def parse_date(date_str, parser: Chronic)
      parser.parse(date_str)&.to_date
    end
  end
end
