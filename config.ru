# This file is used by Rack-based servers to start the application.
$LOAD_PATH.unshift(File.dirname(__FILE__))

require_relative 'config/environment'

require 'app/mind_space_parking_bot'

Thread.abort_on_exception = true
Thread.new do
  MindSpaceParkingBot.run
end


run Rails.application
