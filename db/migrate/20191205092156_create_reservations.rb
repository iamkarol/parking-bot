class CreateReservations < ActiveRecord::Migration[6.0]
  def change
    create_table :reservations do |t|
      t.date :reservation_on, null: false, index: true
      t.references :user, null: false, foreign_key: true, index: true

      t.timestamps
    end
  end
end
